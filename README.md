# CCS CICD Template

# SAST
Below is a quick guide for setting up SAST for an application. Note, this is not exhaustive and may not be enough for more complex applications (multi-domain, APIs etc).
Currently only the default branch results will show in the gitlab gui under security and compliance, so please ensure this is set to whichever branch you want to have the results showing in. I would recommend whichever branch you will be using for test. Otherwise you can download the results as a json artifact attached to the pipeline (CI\\CD -> pipelines -> box on the right -> search for the artifact you want)

### Types of Testing included in this:
<table>
<tr>
<th>Stage name</th>
<th>Description</th>
<th>Step</th>
<th>Contents</th>
</tr>
<tr>
<td>test</td>
<td>SAST is done here.</td>
<td>Before deployment</td>
<td>
</table>

### Includes
<span dir="">include:</span> <span dir="">- remote: 'https://gitlab.com/cogc/public/CCS-CICD-Template/-/raw/main/CCS-SAST.gitlab-ci.yml'</span>

# DAST
Below is a quick guide for setting up DAST for an application. Note, this is not exhaustive and may not be enough for more complex applications (multi-domain, APIs etc).
Currently only the default branch results will show in the gitlab gui under security and compliance, so please ensure this is set to whichever branch you want to have the results showing in. I would recommend whichever branch you will be using for test. Otherwise you can download the results as a json artifact attached to the pipeline (CI\\CD -> pipelines -> box on the right -> search for the artifact you want)

Note DAST has a few known limitations: 
    DAST cannot bypass a CAPTCHA if the authentication flow includes one. Turn these off in the testing environment for the application being scanned.
    DAST cannot handle multi-factor authentication like one-time passwords (OTP) by using SMS, biometrics, or authenticator apps. Turn these off in the testing environment for the application being scanned.
    DAST cannot authenticate to applications that do not set an authentication token during login.
    DAST cannot authenticate to applications that require more than two inputs to be filled out. Two inputs must be supplied, username and password.

DAST documentation: 
https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html

DAST auth documentation: 
https://docs.gitlab.com/ee/user/application_security/dast/authentication.html

### Types of Testing included in this:
<table>
<tr>
<th>Stage name</th>
<th>Description</th>
<th>Step</th>
<th>Contents</th>
</tr>
<tr>
<td>dast</td>
<td>DAST is done here.</td>
<td>After deployment</td>
</tr>
</table>

### Variable options:
<table>
<tr>
<th>Variable Name</th>
<th>Required</th>
<th>Type</th>
<th>Description</th>
<th>Example</th>
</tr>
<tr>
<td>DAST_WEBSITE</td>
<td>Yes</td>
<td>URL</td>
<td>Field used to hold the entry url you want to scan</td>
<td>"https://www.goldcoast.qld.gov.au"</td>
</tr>
<tr>
<td>DAST_AUTH_URL</td>
<td>No</td>
<td>URL</td>
<td>Field used to hold the login page url</td>
<td>"https://www.goldcoast.qld.gov.au"</td>
</tr>
<tr>
<td>DAST_USERNAME</td>
<td>No</td>
<td>string</td>
<td>Field used to hold the entry url you want to scan</td>
<td>"https://www.goldcoast.qld.gov.au"</td>
</tr>
<tr>
<td>DAST_USERNAME_FIELD</td>
<td>No</td>
<td>string</td>
<td>Field used to identify the html element used for the username</td>
<td>"name:username"</td>
</tr>
<td>DAST_PASSWORD</td>
<td>No</td>
<td>string</td>
<td>Field used to hold the entry url you want to scan</td>
<td>"P@55w0rd!"</td>
</tr>
<tr>
<td>DAST_PASSWORD_FIELD</td>
<td>No</td>
<td>string</td>
<td>Field used to identify the html element used for the password</td>
<td>"id:password"</td>
</tr>
<tr>
<td>DAST_FIRST_SUBMIT_FIELD</td>
<td>No</td>
<td>string</td>
<td>Field used if for two step authentication (think Microsoft SSO). This should be the "next" button or whatever is used between entering username and password.Can use ID or name from the html
Note, this isn't MFA / 2FA.</td>
<td>"css:button[type='user-submit']"</td>
</tr>
<tr>
<td>DAST_SUBMIT_FIELD</td>
<td>No</td>
<td>string</td>
<td>Field used to tell DAST what button to click to submit the form</td>
<td>"id:submit"</td>
</tr>
<tr>
<td>DAST_EXCLUDE_URLS</td>
<td>No</td>
<td>urls</td>
<td>Field used to hold a few urls that you don't want to scan with DAST. An example of this would be a logout url in authenticated scans. 
Regular expression syntax can be used to match multiple URLs. This is a comma seperated list</td>
<td>"https://goldcoast.qld.gov.au/ids-sso/v2/session/logout, https://goldcoast.qld.gov.au/ids-sso/v2/session/login"</td>
</tr>
</table>

### Includes
<span dir="">include:</span> <span dir="">- remote: 'https://gitlab.com/cogc/public/CCS-CICD-Template/-/raw/main/CCS-DAST.gitlab-ci.yml'</span>

# DAST API
DAST for API is a little more complex than for a web application. 
As such there is not much pre-configuration possible by the Cyber team, as most of the variables related to the authentication and schema of the API itself. 

### Types of Testing included in this:
<table>
<tr>
<th>Stage name</th>
<th>Description</th>
<th>Step</th>
<th>Contents</th>
</tr>
<tr>
<td>dast</td>
<td>DAST testing for APIs is completed here</td>
<td>After deployment</td>
<td>
</table>

### Includes
<span dir="">include:</span> <span dir="">- remote: 'https://gitlab.com/cogc/public/CCS-CICD-Template/-/raw/main/CCS-DAST-API.gitlab-ci.yml'</span>

### Variable options:
#### Required variables

<table>
<tr>
<th>Variable Name</th>
<th>Required</th>
<th>Type</th>
<th>Description</th>
<th>Example</th>
</tr>
<tr>
<td>DAST_API_TARGET_URL</td>
<td>Yes</td>
<td>URL</td>
<td>Field used to hold the doman you want to scan url you want to scan</td>
<td>"https://www.goldcoast.qld.gov.au"</td>
</tr>
<tr>
<td>DAST_API_OPENAPI</td>
<td>No</td>
<td>URI</td>
<td>Local path and file name for the file holding the OpenAPI formated schema for the API.</td>
<td>"test-api-specification.json"</td>
</tr>
<tr>
<td>DAST_API_HAR</td>
<td>No</td>
<td>URI</td>
<td>Local path and file name for the file holding the HAR (HTTP Archive) formated schema for the API</td>
<td>"test-api-recording.har"</td>
</tr>
<tr>
<td>DAST_API_GRAPHQL</td>
<td>No</td>
<td>URI</td>
<td>path for the GraphQL endpoint</td>
<td>"/api/graphql"</td>
</tr>
<tr>
<td>DAST_API_GRAPHQL_SCHEMA</td>
<td>No</td>
<td>URI</td>
<td>URL for the GraphQL formated schema for the API</td>
<td>"http://file-store/files/test-api-graphql.schema"</td>
</tr>
<tr>
<td>DAST_API_POSTMAN_COLLECTION</td>
<td>No</td>
<td>URI</td>
<td>Local path and file name for the postman collection. It is highly recommended to read about using a postman collection here: https://docs.gitlab.com/ee/user/application_security/dast_api/index.html#dast-api-scanning-with-a-postman-collection-file </td>
<td>"postman-collection_serviceA.json"</td>
</tr>
<tr>
<td>DAST_API_HTTP_USERNAME</td>
<td>No</td>
<td>URI</td>
<td>Username used for basic auth. Note, its not recommended to use basic auth</td>
<td>"testuser"</td>
</tr>
<tr>
<td>DAST_API_HTTP_PASSWORD</td>
<td>No</td>
<td>URI</td>
<td>The variable name used for storing the basic auth secret. Note, this is plane text and not encoded in any way. Not recommended to use basic auth or unencoded text</td>
<td>"$TEST_API_PASSWORD"</td>
</tr>
<tr>
<td>DAST_API_HTTP_PASSWORD_BASE64</td>
<td>No</td>
<td>URI</td>
<td>The variable name used for storing the basic auth secret, but encoded in  base64. Note it is not recommended to use basic authentication</td>
<td>"$TEST_API_PASSWORD"</td>
</tr>
</table>
For bearer tokens: 

 Documentation: https://docs.gitlab.com/ee/user/application_security/dast_api/index.html#bearer-tokens

 Bearer tokens need to be have one of 3 things: 

  1 - A token that doesn't expire

    https://docs.gitlab.com/ee/user/application_security/dast_api/index.html#token-doesnt-expire

  2 - A way to generate a token that lasts as long as the testing

    https://docs.gitlab.com/ee/user/application_security/dast_api/index.html#bearer-tokens

  3 - A python script that DAST API can call to generate the token. 

    https://docs.gitlab.com/ee/user/application_security/dast_api/index.html#token-has-short-expiration
